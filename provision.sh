#!/bin/bash

if [[ "$(whoami)" != "root" ]]
then
  echo "Switching to root..."
  exec sudo -E -- "$0" "$@"
fi

echo "Sleeping for 30 seconds..."
sleep 30

echo "Updating system..."
apt-get -y update
apt-get install apt-transport-https ca-certificates

echo "Add GPG key..."
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 \
  --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

echo "Add repos..."
echo 'deb https://apt.dockerproject.org/repo ubuntu-trusty main' \
  > /etc/apt/sources.list.d/docker.list

export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get purge lxc-docker
apt-cache policy docker-engine
apt-get -y install linux-image-extra-$(uname -r)
apt-get -y install apparmor
apt-get -y install docker-engine=$docker_version-0~trusty

echo "Adding ubuntu user to docker group..."
usermod -aG docker ubuntu

echo "Done."
