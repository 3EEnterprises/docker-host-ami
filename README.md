## docker-host-ami

[Packer](https://www.packer.io/) code to create a docker host AMI on AWS.

To use:

```bash
packer build \
  -var brand=$YOUR_BRAND \
  -var docker_version=1.10.3 \
  -var revision=0 \
  -var source_ami=ami-df6a8b9b \
  -var aws_region=us-west-1 \
  -var aws_access_key=$AWS_ACCESS_KEY_ID \
  -var aws_secret_key=$AWS_SECRET_ACCESS_KEY \
  docker-host.json
```

Pre-generated AMIs are available as `3E-ubuntu-14.04-docker-1.10.3.0` in all AWS regions except `us-gov-west-1` and `cn-north-1` which are both restricted/invite-only regions.

Specific AMIs:

ap-northeast-1: ami-87f9ebe9
ap-northeast-2: ami-5bd91135
ap-southeast-1: ami-6954810a
ap-southeast-2: ami-c91c3eaa
eu-central-1: ami-6dec0c02
eu-west-1: ami-62d85a11
sa-east-1: ami-a0d35dcc
us-east-1: ami-787d6e12
us-west-1: ami-a5cfb3c5
us-west-2: ami-e1906781

Packer source inspired by [https://github.com/d11wtq/docker-ami-packer](https://github.com/d11wtq/docker-ami-packer).

Questions/comments: @ErikEvenson on twitter.
